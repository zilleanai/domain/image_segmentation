# image_segmentation

image_segmentation domain

## Installation

```bash
pip install git+https://github.com/zilleanai/zillean_cli
zillean-cli domain install https://gitlab.com/zilleanai/domain/image_segmentation.git
```

## Example

Create 'example' in Projects.
Go to 'Files' and download an image in train folder.
Go to 'Workflow', 'Predict image' and paste downloaded image there.
