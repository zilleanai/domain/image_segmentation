# source: https://github.com/yunjey/pytorch-tutorial/blob/master/tutorials/01-basics/feedforward_neural_network/main.py#L37-L49
import os
import io
from random import randint
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.autograd import Variable

from zworkflow.evaluate import EvaluateBase


class evaluate(EvaluateBase):

    def __init__(self, config):
        super().__init__(config)
        self.device = torch.device(self.config['train']['device'])

    def evaluate(self, dataset, model):
        net = model.net()
        net.to(self.device)
        model.load()
        net.eval()
        idx = randint(0, len(dataset))
        with torch.no_grad():
            X = dataset[idx]
            if len(X) == 2:
                X = X[0]
            X = torch.stack([torch.from_numpy(X)])
            X = X.to(self.device)
            y = net(X).cpu()
            im = torch.sigmoid(y)
            im = im[0].numpy()[0]
            im = (im > 0.5).astype(np.uint8)
            im = im * 255
            im = np.squeeze(im)
            figure, axarr = plt.subplots(1, 2, figsize=(16, 8))
            image = dataset.load_image(dataset.images[idx])
            axarr[0].imshow(image)
            axarr[1].imshow(im)

            figure.savefig('evaluate.png')
            figure.show()
            return {'images': ['evaluate.png']}
        return {}

    def __str__(self):
        return 'ternaus evaluate'
