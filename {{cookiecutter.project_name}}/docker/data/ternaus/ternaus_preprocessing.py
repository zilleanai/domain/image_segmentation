
import cv2
from zworkflow.preprocessing import PreprocessingBase


class preprocessing(PreprocessingBase):
    def __init__(self, config):
        super().__init__(config)
        self.functions['pad'] = self.pad
        self.functions['resize'] = self.resize
        self.functions['normalize'] = self.normalize
        self.functions['classify_mask'] = self.classify_mask

        self.width = self.config['dataset']['width']
        self.height = self.config['dataset']['height']

    def pad(self, data):
        (img, mask) = data
        height, width, _ = img.shape
        
        if height % 32 == 0:
            y_min_pad = 0
            y_max_pad = 0
        else:
            y_pad = 32 - height % 32
            y_min_pad = int(y_pad / 2)
            y_max_pad = y_pad - y_min_pad
            
        if width % 32 == 0:
            x_min_pad = 0
            x_max_pad = 0
        else:
            x_pad = 32 - width % 32
            x_min_pad = int(x_pad / 2)
            x_max_pad = x_pad - x_min_pad
        
        img = cv2.copyMakeBorder(img, y_min_pad, y_max_pad, x_min_pad, x_max_pad, cv2.BORDER_REFLECT_101)
        if mask is None:
            return (img, None)
        mask = cv2.copyMakeBorder(mask, y_min_pad, y_max_pad, x_min_pad, x_max_pad, cv2.BORDER_REFLECT_101)
        return (img, mask)

    def normalize(self, data):
        # source: https://pytorch.org/docs/stable/torchvision/models.html
        (img, mask) = data
        normed_img = ((img - [0.485, 0.456, 0.406]) / [0.229, 0.224, 0.225])
        return (normed_img, mask)

    def resize(self, data):
        (img, mask) = data
        img = cv2.resize(
            img, (self.height, self.width))
        if mask is None:
            return (img, None)
        mask = cv2.resize(
            mask, (self.height, self.width), interpolation=cv2.INTER_NEAREST)
        return (img, mask)

    def classify_mask(self, data):
        (img, mask) = data
        if mask is None:
            return data
        mask[mask != 0] = 1
        mask[mask == 0] = 0
        return (img, mask)

    def process(self, data, verbose=False):
        for f in self.config['preprocessing']['functions']:
            fun = self.functions[f]
            data = fun(data)
        return data

    def __str__(self):
        return "segmentation preprocessing: " + str(list(self.functions.keys()))
