# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

FROM jupyter/datascience-notebook

# Install jupyterlab
ARG JUPYTERLAB_VERSION=0.35.6
RUN conda install -c conda-forge jupyterlab --yes
RUN jupyter serverextension enable --py jupyterlab --sys-prefix
RUN pip install jupyterlab==$JUPYTERLAB_VERSION \
    &&  jupyter labextension install @jupyterlab/hub-extension
RUN pip install pandas XlsxWriter

USER jovyan
COPY start.ipynb /home/jovyan/work/start.ipynb
