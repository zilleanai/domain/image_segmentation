import React from 'react'
import { Route, Switch } from 'react-router-dom'
import startCase from 'lodash/startCase'
import { compile } from 'path-to-regexp'

import {
  ForgotPassword,
  Login,
  Logout,
  PendingConfirmation,
  Profile,
  SignUp,
  ResendConfirmation,
  ResetPassword,
} from 'security/pages'

import {
  Contact,
  Home,
  NotFound,
  Styles,
} from 'site/pages'

import {
  Projects
} from 'comps/project/pages'

import {
  TagDetail,
  Tags
} from 'comps/tag/pages'

import {
  Files
} from 'comps/file/pages'

import {
  Git
} from 'comps/git/pages'

import {
  ImageUpload
} from 'comps/image/pages'
import {
  RecordStream
} from 'comps/record_stream/pages'

import {
  Workflow,
} from 'comps/workflow/pages'
import {
  Train,
} from 'comps/train/pages'
import {
  Labeling,
} from 'comps/labeling/pages'
import {
  LabelingMask,
  LabelingMaskPainter,
} from 'comps/labeling_mask/pages'
import {
  Preprocessing,
} from 'comps/preprocessing/pages'
import {
  Evaluate,
} from 'comps/evaluate/pages'
import {
  Predict,
} from 'comps/predict/pages'
import {
  PredictImage,
} from 'comps/predict_image/pages'
import {
  DockerRunner,
} from 'comps/docker_runner/pages'
import {
  DocDetail,
  Docs
} from 'comps/docs/pages'

import { AnonymousRoute, ProtectedRoute } from 'utils/route'


/**
 * ROUTES: The canonical store of frontend routes. Routes throughout the system
 * should be referenced using these constants
 *
 * Both keys and values are component class names
 */
export const ROUTES = {
  Contact: 'Contact',
  ForgotPassword: 'ForgotPassword',
  Home: 'Home',
  Login: 'Login',
  Logout: 'Logout',
  PendingConfirmation: 'PendingConfirmation',
  Profile: 'Profile',
  ResendConfirmation: 'ResendConfirmation',
  ResetPassword: 'ResetPassword',
  SignUp: 'SignUp',
  Styles: 'Styles',
  Projects: 'Projects',
  TagDetail: 'TagDetail',
  Tags: 'Tags',
  Files: 'Files',
  Git: 'Git',
  ImageUpload: 'ImageUpload',
  RecordStream: 'RecordStream',
  Workflow: 'Workflow',
  Train: 'Train',
  Labeling: 'Labeling',
  LabelingMask: 'LabelingMask',
  LabelingMaskPainter: 'LabelingMaskPainter',
  Preprocessing: 'Preprocessing',
  Evaluate: 'Evaluate',
  Predict: 'Predict',
  PredictImage: 'PredictImage',
  DockerRunner: 'DockerRunner',
  DocDetail: 'DocDetail',
  Docs: 'Docs',
}

/**
 * route details
 *
 * list of objects with keys:
 *  - key: component class name
 *  - path: the path for the component (in react router notation)
 *  - component: The component to use
 *  - routeComponent: optional, AnonymousRoute, ProtectedRoute or Route (default: Route)
 *  - label: optional, label to use for links (default: startCase(key))
 */
const routes = [
  {
    key: ROUTES.Contact,
    path: '/contact',
    component: Contact,
  },
  {
    key: ROUTES.ForgotPassword,
    path: '/login/forgot-password',
    component: ForgotPassword,
    routeComponent: AnonymousRoute,
    label: 'Forgot password?',
  },
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  {
    key: ROUTES.Login,
    path: '/login',
    component: Login,
    routeComponent: AnonymousRoute,
    label: 'Login',
  },
  {
    key: ROUTES.Logout,
    path: '/logout',
    component: Logout,
    label: 'Logout',
  },
  {
    key: ROUTES.PendingConfirmation,
    path: '/sign-up/pending-confirm-email',
    component: PendingConfirmation,
    routeComponent: AnonymousRoute,
    label: 'Pending Confirm Email',
  },
  {
    key: ROUTES.Profile,
    path: '/profile',
    component: Profile,
    routeComponent: ProtectedRoute,
    label: 'Profile',
  },
  {
    key: ROUTES.ResendConfirmation,
    path: '/sign-up/resend-confirmation-email',
    component: ResendConfirmation,
    routeComponent: AnonymousRoute,
    label: 'Resend Confirmation Email',
  },
  {
    key: ROUTES.ResetPassword,
    path: '/login/reset-password/:token',
    component: ResetPassword,
    routeComponent: AnonymousRoute,
    label: 'Reset Password',
  },
  {
    key: ROUTES.SignUp,
    path: '/sign-up',
    component: SignUp,
    routeComponent: AnonymousRoute,
    label: 'Sign Up',
  },
  {
    key: ROUTES.Styles,
    path: '/styles',
    component: Styles,
  },
  {
    key: ROUTES.Projects,
    path: '/projects',
    component: Projects,
  },
  {
    key: ROUTES.TagDetail,
    path: '/tags/:name',
    component: TagDetail,
  },
  {
    key: ROUTES.Tags,
    path: '/tags',
    component: Tags,
  },
  {
    key: ROUTES.Files,
    path: '/files/:subpath',
    component: Files,
  },
  {
    key: ROUTES.Git,
    path: '/git',
    component: Git,
  },
  {
    key: ROUTES.ImageUpload,
    path: '/imageupload/:subpath',
    component: ImageUpload,
  },
  {
    key: ROUTES.RecordStream,
    path: '/record_stream',
    component: RecordStream,
  },
  {
    key: ROUTES.Workflow,
    path: '/workflow',
    component: Workflow,
  },
  {
    key: ROUTES.Train,
    path: '/train',
    component: Train,
  },
  {
    key: ROUTES.Labeling,
    path: '/labeling',
    component: Labeling,
  },
  {
    key: ROUTES.LabelingMask,
    path: '/labeling_mask/:subpath',
    component: LabelingMask,
  },
  {
    key: ROUTES.LabelingMaskPainter,
    path: '/labeling_mask/:path/:file',
    component: LabelingMaskPainter,
  },
  {
    key: ROUTES.Preprocessing,
    path: '/preprocessing',
    component: Preprocessing,
  },
  {
    key: ROUTES.Evaluate,
    path: '/evaluate',
    component: Evaluate,
  },
  {
    key: ROUTES.Predict,
    path: '/predict',
    component: Predict,
  },
  {
    key: ROUTES.PredictImage,
    path: '/predict_image',
    component: PredictImage,
  },
  {
    key: ROUTES.DockerRunner,
    path: '/docker_runner',
    component: DockerRunner,
  },
  {
    key: ROUTES.Docs,
    path: '/docs',
    component: Docs,
  },
]

/**
 * ROUTE_MAP: A public lookup for route details by key
 */
export const ROUTE_MAP = {}
routes.forEach((route) => {
  let { component, key, label, path, routeComponent } = route

  if (!component) {
    throw new Error(`component was not specified for the ${key} route!`)
  }
  if (!path) {
    throw new Error(`path was not specified for the ${key} route!`)
  }

  ROUTE_MAP[key] = {
    path,
    toPath: compile(path),
    component,
    routeComponent: routeComponent || Route,
    label: label || startCase(key),
  }
})

/**
 * React Router 4 re-renders all child components of Switch statements on
 * every page change. Therefore, we render routes ahead of time once.
 */
const cachedRoutes = routes.map((route) => {
  const { component, path, routeComponent: RouteComponent } = ROUTE_MAP[route.key]
  return <RouteComponent exact path={path} component={component} key={path} />
})
cachedRoutes.push(<Route component={NotFound} key="*" />)

export default () => (
  <Switch>
    {cachedRoutes}
  </Switch>
)


