FROM zilleanai/zillean_backend
USER root
RUN pip install -U --no-cache-dir -e git+https://github.com/briancappello/flask-unchained.git#egg=flask-unchained
COPY zillean-domain.yml zillean-domain.yml
RUN zillean-cli domain install_requirements zillean-domain.yml --no_js
RUN pip uninstall -y opencv-python
RUN apt update && apt install -y python3-opencv ffmpeg x264 v4l-utils
RUN pip install imgaug
USER flask
COPY unchained_config.py unchained_config.py
COPY routes.py backend/routes.py
COPY config.py backend/config.py
COPY bundles bundles
COPY docker/data data

