from flask_unchained import (controller, resource, func, include, prefix,
                             get, delete, post, patch, put, rule)

from .views import SiteController

routes = lambda: [
    include('bundles.project.routes'),
    include('bundles.tag.routes'),
    include('bundles.file.routes'),
    include('bundles.git.routes'),
    include('bundles.image.routes'),
    include('bundles.record_stream.routes'),
    include('bundles.workflow.routes'),
    include('bundles.train.routes'),
    include('bundles.labeling.routes'),
    include('bundles.labeling_mask.routes'),
    include('bundles.preprocessing.routes'),
    include('bundles.evaluate.routes'),
    include('bundles.predict.routes'),
    include('bundles.predict_image.routes'),
    include('bundles.docs.routes'),
    include('bundles.pyclient.routes'),
    include('bundles.docker_runner.routes'),
    controller(SiteController), 
]
