import React from 'react'
import ReactSVG from 'react-svg'

import es6Logo from './es6.png'
import reactLogo from './react.svg'
import reactRouterLogo from './react-router.png'
import reduxLogo from './redux.svg'
import reduxSagaLogo from './redux-saga.svg'
import reduxFormLogo from './redux-form.png'
import sassLogo from './sass.svg'
import webpackLogo from './webpack.svg'
import babelLogo from './babel.svg'

import pythonLogo from './python.svg'
import flaskLogo from './flask.svg'
import flaskSecurityLogo from './flask-security.svg'
import sqlalchemyLogo from './sqlalchemy.svg'
import flaskRestfulLogo from './flask-restful.png'
import marshmallowLogo from './marshmallow.png'
import flaskMailLogo from './flask-mail.png'
import celeryLogo from './celery.svg'

import ansibleLogo from './ansible.svg'
import centosLogo from './centos.svg'
import postgresLogo from './postgresql.svg'
import redisLogo from './redis.svg'
import nginxLogo from './nginx.svg'
import uwsgiLogo from './uwsgi.png'
import letsEncryptLogo from './letsencrypt.svg'
import postfixLogo from './postfix.svg'


const Logo = ({ children, maxWidth }) => (
  <div className="logo" style={ { display: 'inline-block', paddingRight: '15px' } }>
    {children}
  </div>
)

const SvgLogo = ({ logo, label, maxWidth='110px' }) => (
  <Logo maxWidth={maxWidth}>
    <div className="center" style={ { width: maxWidth } }>
      <ReactSVG path={logo} style={ { height: '100px', maxWidth } } />
    </div>
    <div className="center">{label}</div>
  </Logo>
)

const PngLogo = ({ logo, label, maxWidth='110px', style }) => (
  <Logo maxWidth={maxWidth}>
    <div className="center">
      <img src={logo} style={ { maxHeight: '100px', maxWidth, ...style } } />
    </div>
    <div className="center">{label}</div>
  </Logo>
)


export default (props) => (
  <div>
    <h1>Welcome to Zillean - Image Segmentation!</h1>
    
  </div>
)
