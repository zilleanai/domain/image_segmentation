import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import classnames from 'classnames'

import { ROUTES } from 'routes'
import { storage } from 'comps/project'
import NavLink from './NavLink'

import './navbar.scss'


class NavBar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      menuOpen: false,
      dataOpen: false,
      workflowOpen: false,
    }
  }

  componentWillReceiveProps() {
    this.setState({ menuOpen: false })
  }

  render() {
    const { isAuthenticated } = this.props
    const { menuOpen, dataOpen, workflowOpen } = this.state

    return (
      <nav className={classnames({ 'menu-open': menuOpen })}>
        <div className="container navbar-top">
          <NavLink exact to={ROUTES.Home} className="brand">
            <span className="tld">Zillean</span>({storage.getProject()})
          </NavLink>
          <a href="javascript:void(0);"
            className="burger"
            onClick={this.toggleResponsiveMenu}
          >
            Menu&nbsp;&nbsp;&#9776;
          </a>
          <div className="menu left">
            <NavLink to={ROUTES.Projects} />
            <NavLink to={ROUTES.Git} />
            <NavLink to={ROUTES.Tags} />
            <a href="javascript:void(0);"
              onClick={this.toggleDataMenu}
            >
              Data
            </a>
            <a href="javascript:void(0);"
              onClick={this.toggleWorkflowMenu}
            >
              Workflow
            </a>
            <NavLink to={ROUTES.DockerRunner} />
            <a href="/notebook/">Notebook</a>
            <NavLink to={ROUTES.Docs} />
          </div>
	  <div className="menu left">{dataOpen ? this.renderDataMenu() : null}</div>
          <div className="menu left">{workflowOpen ? this.renderWorkflowMenu() : null}</div>
        </div>
      </nav>
    )
  }

  toggleResponsiveMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen })
  }

  toggleDataMenu = () => {
    this.setState({ dataOpen: !this.state.dataOpen })
  }

  toggleWorkflowMenu = () => {
    this.setState({ workflowOpen: !this.state.workflowOpen })
  }

  renderDataMenu() {
    return (
      <nav>
            <NavLink to={ROUTES.Files} params={ { subpath: '/' }} />
            <NavLink to={ROUTES.ImageUpload} params={ { subpath: '/' }} />
            <NavLink to={ROUTES.RecordStream} />
      </nav>
    )
  }

  renderWorkflowMenu() {
    return (
      <nav>
	<NavLink to={ROUTES.Workflow} />
        <NavLink to={ROUTES.Labeling} />
	<NavLink to={ROUTES.LabelingMask} params={ { subpath: '/' }} />
        <NavLink to={ROUTES.Preprocessing} />
        <NavLink to={ROUTES.Train} />
	<NavLink to={ROUTES.Evaluate} />
        <NavLink to={ROUTES.Predict} />
        <NavLink to={ROUTES.PredictImage} />
      </nav>
    )
  }

  renderAuthenticatedMenu() {
    return (
      <div>
        <NavLink to={ROUTES.Profile} />
        <NavLink to={ROUTES.Logout} />
      </div>
    )
  }

  renderUnauthenticatedMenu() {
    return (
      <div>
        <NavLink to={ROUTES.SignUp} />
        <NavLink to={ROUTES.Login} />
      </div>
    )
  }
}

const withConnect = connect(
  (state) => ({ isAuthenticated: state.security.isAuthenticated }),
)

export default compose(
  withRouter,  // required for NavLinks to determine whether they're active or not
  withConnect,
)(NavBar)



